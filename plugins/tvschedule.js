/**
 * Project : Jarvis-Node
 * Author  : wjarosz
 * Date    : 5/09/2014
 * Time    : 2:21 PM
 */


/**
 * install = node-wolfram
 * install = say
 * install = xml2js
 * install = url
 */
var sys = require('sys');
var fs = require('fs');
var url = require('url');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var events = require('events');
var say    = require('say');
var xml2js = require('xml2js');
var zlib = require('zlib');
var _ = require('lodash');


var request = require('request'),
    out = fs.createWriteStream('out');

var parser = new xml2js.Parser();
var exports = module.exports = {};

module.exports = function(phrase,callback) {
    var  self = this,
         speaking = 0,
         voice = 'Alex',
         $channels = ['7TWO-NSW', '7mate', 'ABC-NSW', 'ABC-News24', 'ABC2', 'ABC3', 'ELEVEN', 'GEM-NSW', 'GO', 'Nine-Syd', 'One-NSW', 'SBS-NSW', 'SBSTWO-NSW', 'Seven-Syd', 'Ten-NSW'],
         $currentDate = new Date(),
         day = $currentDate.getDate(),
         month = $currentDate.getMonth() + 1,
         year = $currentDate.getFullYear(),
         hour = $currentDate.getHours(),
         eventEmitter = new events.EventEmitter(),
         DOWNLOAD_DIR = './downloads/';
         schedule = [];
         found = [];

    month = month > 9 ? month : "0" + month;
    day = day > 9 ? day : "0" + day;
    hour = hour > 9 ? hour : '0' + hour;


    String.prototype.toTitleCase = function () {
        return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    };

    var puts = function (error, stdout) {
        sys.puts(stdout);
    };

    var processMessage = function(response){
        if (!callback && speaking === 0) {
            speaking++;
            say.speak(voice, response, function () {
                speaking = 0;
            });
        } else {
            callback(response) ;
        }
    };

    var search = function(obj, name) {
        /*if ( typeof obj.category == 'object'){
            console.log(obj.category + ' ' + obj.category.indexOf(name));
        }
        */
        if ( typeof obj.category == 'object' && obj.category.indexOf(name) > -1) {
            return obj;
        }
        /*if (obj.children || obj._children) {
            var ch = obj.children || obj._children;
            for (var i = 0; i < ch.length; i++) {
                var found = search(ch[i], name);
                if (found) {
                    return found;
                }
            }
        }*/
        return false;
    };

    var download = function(file_url,counter,xmlFile) {
        // extract the file name
        var file_name = url.parse(file_url).pathname.split('/').pop();
        fs.exists(DOWNLOAD_DIR + '/' + xmlFile,function(xmlExist){
           if (!xmlExist){
               fs.exists(DOWNLOAD_DIR + '/' + file_name, function(exists) {
                   if (!exists) {
                       //remove all folder content and download files
                       exec('rm ' + DOWNLOAD_DIR + '/*',puts);
                       var wget = 'wget -P ' + DOWNLOAD_DIR + ' ' + file_url;
                       exec(wget, function(err, stdout, stderr) {
                           if (err) { throw err; }
                           else {
                               eventEmitter.emit('ungzip',counter,file_name);
                           }
                       });
                   } else {
                       eventEmitter.emit('ungzip',counter,file_name);
                   }
               });
           } else {

               if (counter === size ){

                   eventEmitter.emit('xmlToJson');
               }
           }
        });

    };

    var i = 0,
        size = $channels.length;
    for (i; i < size; i++) {
        var fileNameRead = $channels[i] + '_' + year + '-' + month + '-' + day + '.xml.gz';
        var fileNameWrite = $channels[i] + '_' + year + '-' + month + '-' + day + '.xml';
        var file_url = 'http://xml.oztivo.net/xmltv/' + fileNameRead;
        download(file_url,i+1,fileNameWrite);
    }


    eventEmitter.on('ungzip',function(counter,file_name){

        var file = DOWNLOAD_DIR + '/' + file_name;
        exec('gunzip ' + file, puts);
        console.log(counter + ' ' + size);
        if ( counter === size){
            //give him a sec to finish
            //setTimeout( function(){

                eventEmitter.emit('xmlToJson');

            //},1000);
        }


    });

    eventEmitter.on('xmlToJson',function(){
        var i = 0,
            size = $channels.length;

        for (i; i < size; i++) {
            var fileName = $channels[i] + '_' + year + '-' + month + '-' + day + '.xml';
            (function(counter){
                fs.readFile( DOWNLOAD_DIR + fileName, function(err, data) {
                    parser.parseString(data, function (err, result) {
                        schedule.push(result.tv.programme);
                    });

                    if (i === size){

                        eventEmitter.emit('searching');
                    }
                });

            })(i)
        }
    });

    eventEmitter.on('searching',function(){
        schedule.forEach( function(obj,counter) {

            obj.forEach( function(el,c) {

                if ( search(el,phrase.toTitleCase()) !== false ){
                    found.push(search(el,phrase.toTitleCase()));
                };
                if (counter === schedule.length-1 && c === obj.length-1  ){
                    if ( _.isEmpty(found) === false) {
                        eventEmitter.emit('found');
                    } else {
                        var response = [];
                            response[0] = 'No ' + phrase + ' within 6 hours on channel ' +  el.$.channel;
                        eventEmitter.emit('done', response);
                    }

                }
            });
        });
    });

    eventEmitter.on('found',function(){
       //only 6h in future. Following format : 20140906190000 +1000
       var currentTime = new Date();
       var next3h = currentTime.getHours() + 6;
           next3h = next3h > 9 ? next3h : '0' + h;
       var currH = currentTime.getHours();
           currH = currH > 9 ? currH : '0' + currH;
       var min = currentTime.getMinutes();
           min = min > 9 ? min : '0'+min;
       var nextThreeHours = year + '' + month + '' + day + '' + next3h + '' + min + '00';

       var now = year + '' + month + '' + day + '' + currH + '' + min + '00';
       var response = [];
       var _parse = function(str) {
            var h = str.substr(8,2),
                m = str.substr(10,2),
                timer;
            var num = Number(m);

            switch (true){
                case (num==0):
                    timer = h;
                    break;
                case (num<=30) :
                    timer = num + ' past ' + h;
                    break;
                case (num>30) :
                    var remainder = num - 30;
                    var next = (h=='23') ? '0' : Number(h) + 1;
                    timer = remainder + ' to ' + next;
                    break;

            }

            return timer;
       };

       if (_.isEmpty(found) === true){
           response[0] = 'there is no ' + phrase + ' within 6 hours on channel ';
           eventEmitter.emit('done', response);
       }

       found.forEach(function(obj,counter){
           var showTime = obj.$.start.replace(/\s\+(.+)$/g,'');
           if (Number(showTime) >= Number(now) && Number(showTime) <= Number(nextThreeHours)){ // < Number(nextThreeHours) && Number(showTime) > Number(now)) {
               response.push(obj.title + ' on ' + obj.$.channel + ' today at ' + _parse(showTime) );
               if (counter === found.length - 1) {
                   eventEmitter.emit('done', response);
               }
           } else {
               //response[0] = 'there is no ' + phrase + ' within 6 hours on '+ obj.$.channel;
           }
       });



    });

    eventEmitter.on('done', function(res){
        var f = _.unique(res);
        var r = _(f).forEach().join('[[slnc 500]]');

        processMessage(r);
    });



};