/**
 * Project : Jarvis-Node
 * Author  : wjarosz
 * Date    : 3/09/2014
 * Time    : 4:37 PM
 */

/**
 * install = url
 * install = request
 */
var url = require('url'),
    request = require('request'),
    exec = require('child_process').exec;

var puts = function(error, stdout) {
    sys.puts(stdout);
};

var exports = module.exports = {};

module.exports = function(res) {

    var call = url.parse("http://gdata.youtube.com/feeds/api/videos?q="+res+"&alt=json");
    var u = url.format(call);
    request(u, function(err, response, body) {
        var res = JSON.parse(body);
        var u = res.feed.entry[0].link[0].href;
        var link = u.match(/v=([^&]*)/ig);
        exec('python -m webbrowser -t "http://youtube.com/watch?'+link+'"',puts);
    });


};