/**
 * Project : Jarvis-Angular-Yo
 * Author  : wjarosz
 * Date    : 31/08/2014
 * Time    : 6:15 PM
 */

/**
 * install = node-wolfram
 * install = say
 */
 var client = require('node-wolfram');
 var say    = require('say');

 var exports = module.exports = {};
 var wolfram = new client('WEATQH-EJ6AKUPPK7');

 module.exports = function(res,callback) {
     var speaking = 0;
     var voice = 'Alex';

     var processMessage = function(response){
         if (!callback) {
             speak++;
             say.speak(voice, response, function () {
                 speak = 0;
             });
         } else {
             callback(response) ;
         }
     };


    wolfram.query(res, function(err, result) {
        if(err)
            processMessage(err);
        else {
            if(result.queryresult.$.success == 'true' && result.queryresult.pod && result.queryresult.pod.length >= 1){
                var text = result.queryresult.pod[1].subpod[0].plaintext[0];
                if (speaking === false)
                    processMessage(text);
            } else {
                if (speaking === false)
                    processMessage("I am not able to find results for " + res);
            }
        }
    });
};