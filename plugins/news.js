/**
 * Project : Jarvis-Node
 * Author  : wjarosz
 * Date    : 3/09/2014
 * Time    : 4:35 PM
 */

/**
 * install = url
 * install = say
 * install = request
 */
var url = require('url');
var request = require('request');
var say    = require('say');

var exports = module.exports = {};

module.exports = function(callback) {
    var voice = 'Alex',
        speaking = 0;

    var processMessage = function(response){
        if (!callback && speaking === 0) {
            speaking++;
            say.speak(voice, response, function () {
                speaking = 0;
            });
        } else {
            callback(response);
        }
    };

    var url = require('url');
    var request = require('request');
    var call = url.parse("http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=8&q=http%3A%2F%2Fnews.google.com%2Fnews%3Fned%3Dau%26output%3Drss");
    var u = url.format(call);
    var news = '';
    request(u, function(err, response, body) {
        var res = JSON.parse(body);
        res.responseData.feed.entries.forEach(function(o){
            var title = o.title.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            var splitted = title.split('  ');
            var heading = splitted[0];
            var source = splitted[1];
            var text = source + "<silence msec=\"1000\"/>" + title + " " + heading;
            news += source + "" + heading + "  ";
        });
            processMessage(news);
    });
};
