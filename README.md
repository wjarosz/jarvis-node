R-PI INSTALLATION NOTES

STEPS : 
Download image from  http://www.raspberrypi.org/help/noobs-setup/ Ive used Noob version
Download sd formatter  https://www.sdcard.org/downloads/formatter_4/eula_mac/ or play around with http://elinux.org/RPi_Easy_SD_Card_Setup#Run_an_App_.28Only_with_graphical_interface.29
Format your SD card (override type)
Unzip Noob files and drop to newly formatted SD
Connect everything. Wait unit it finish installing raspberrian (you can choose few other images)
Plug in WIFI , rpi will reboot 
open /etc/network/interfaces and update as following : 
		auto wlan0
 		face wlan0 inet DHCP
			wpa-ssid [your ssid]
			wpa-psk  [password]

		sudo ifdown wlan0
		sudo ifup wlan0
Update apt
		sudo apt-get update
		sudo apt-get upgrade
		sudo apt-get dist-upgrade
Update firmware from https://github.com/Hexxeh/rpi-update I didnt do it ;)

Install lira
sudo apt-get install lirc
update /etc/modules with following lines
lirc_dev
lirc_rpi gpio_in_pin=[recorder pin] gpio_out_pin=[IR pin]
update /etc/lirc/hardware.conf with following 
LIRCD_ARGS=“—uinput”
DRIVER=“default”
DEVICE=“/dev/lirc0”
MODULES=“lirc_rpi”
restart and execute following
sudo /etc/init.d/lirc stop
mode2 -d /dev/lirc0  [it should print signals] if yes quit
irrecord -d /dev/lirc0 ~/lirc0.conf
sudo mv /etc/lirc/lircd.conf /etc/lirc/lircd_orginal.conf
sudo cp ~/lircd.conf /etc/lirc/lircd.conf
update manually lircd.conf put your config name in name line
sudo /etc/init.d/lirc start
lnstall node
               sudo su 
		   wget http://nodejs.org/dist/v0.10.2/node-v0.10.2.tar.gz
		tar -xzf node-v0.10.2.tar.gz
		cd node-v0.10.2
		./configure
		make
		make install

Clone repo from bitbucket and run npm update
apt-get install festival for tts

Install better voices http://ubuntuforums.org/showthread.php?t=677277