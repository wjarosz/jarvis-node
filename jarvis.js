/**
 * Includes
 * Dependencies  :  python, java
 *
 * @Todo queue processor for executing task one after another. Will be
 * @Todo Observer pattern for checking if speaking is finished and fired voiceRecorder
 * @Todo Add break command, stop process if user ask Jarvis to do so
 * @Todo allow callbacks on plugins. Eg. pass processMessage as callback to plugin and let him talk
 * @Todo plugins torrent download, chromecast, gmail, calendar check,
 *
 **/
(function () {
    var sys = require('sys'),
        exec = require('child_process').exec,
        fs = require('fs'),
        util = require('util'),
        say = require('say'),
        speaker = require('speakable'),
        path = require('path'),
        _ = require('lodash'),
        http = require('http'),
        express = require('express'),
        request = require('request'),
        cors = require('cors'),
        Busboy = require('busboy'),
        bodyParser = require('body-parser'),
        sockjs = require('sockjs'),
        pty = require('pty');

    /**
     * Globals
     */
    var commands = [],
        config = {},
        currentCommand = {},
        fn = {},
        speaking = false,
        commandsFile = "commands.txt",
        configFile = "config.txt",
        speakable = new speaker(
            {   key: 'AIzaSyDUxQD-3IvU7LclGxkqvkqQjRCWcMx57ok' },
            {   threshold: 0 }
        ),
        app = express(),
        socketServer = sockjs.createServer();

    var welcome = function () {
        var day = new Date();
        var hr = day.getHours();
        var dayTime = '';
        if (hr < 12) {
            dayTime = 'Good morning';
        }
        else if (hr > 12 && hr < 18) {
            dayTime = 'Good afternoon';
        }
        else {
            dayTime = 'Good evening';
        }
	    processMessage(dayTime + " " + config.owner + " " + config.welcomeText);
    };

    var puts = function (error, stdout) {
        sys.puts(stdout);
    };

    /**
     * Register plugins
     */
    var pluginRegistration = function () {
        var dir = __dirname + '/plugins/';
        var data = {};

        fs.readdir(dir, function (err, files) {
            if (err) throw err;
            files.forEach(function (file) {
                fs.readFile(dir + file, 'utf-8', function (err, html) {
                    if (err) throw err;
                    data[file] = html;
                    fn[file.replace(/(.js)/gi, '')] = require(dir + file);
                });
            });
        });
    };

    /**
     *  Get commands from text file.
     **/
    var getCommands = function () {
        commands = [];
        fs.readFile(commandsFile, 'utf8', function (err, data) {
            if (err) throw err;
            var eachLine = data.split('\n');
            eachLine.forEach(function (obj) {
                registerTask(obj);
            });
        });
    };

    /**
     * Get config variables from text file
     */
    var getConfig = function () {
        fs.readFile(configFile, 'utf8', function (err, data) {
            if (err) throw err;
            var eachLine = data.split('\n');
            eachLine.forEach(function (obj) {
                var conf = obj.split('=');
                var val = (conf[1] !== undefined) ? conf[1].replace(/\s/, '') : '';
                var key = conf[0].replace(/\s/g, '');
                config[key] = val;
            });
        });
    };

    /**
     *  Register function from config file
     *  Split array into 2+params pieces.
     *  @param String task (line of text from config file)
     *
     **/
    var registerTask = function (task) {
        var t = task.split("==");
        var o = {};

        t.forEach(function (e, k) {
            if (k === 0) {
                o.command = e;
            } else {
                o.param = e;
            }
        });
        commands.push(o);
    };

    /**
     * Speaker
     * @param String response
     **/
    var processMessage = function (response) {
        speaking = true;
        say.speak(null, response, function (data) {
            speaking = false;
            console.log(speaking);
        });

    };

    /**
     * Main algorithm.
     * Iterates over user defined commands, as argument receive list of users
     * words. Match them against commands, the more matches the heavies method become.
     * The heaviest method is executed. Either with or without extra parameters represented by [...]
     * @param String words
     *
     **/
    var commandStack = function (words) {
        var spokenWords = words.split(' ');
        commandsQueue = [];
        tempA = [];

        commands.forEach(function (c, k) {
            var command = c.command.split("|");
            if (command[0].match(new RegExp(spokenWords.join("|"), "ig"))) {
                var weight = 0;
                var par = [];
                var tempA = [];
                c.command.split(" ").forEach(function (spl) {
                    //calculate how many matches does the search word apear in defined commands
                    if (words.indexOf(spl) > -1) {
                        weight++;
                        if (tempA.indexOf(spl) === -1) {
                            tempA.push(spl);
                        }
                    }
                });
                if (c.command.match(new RegExp(tempA.join(" ")))) {
                    if (c.param.match(/\(()\)/gi) && commandsQueue.indexOf(c.param.replace('()', '') === -1)) {
                        commandsQueue.push(
                            {   'callback': c.param.replace('()', ''),
                                'weight': weight,
                                'type': 'fn',
                                'extras': command[1]
                            }
                        );
                    }
                    else if (c.param.match(/\((...)\)/gi) && commandsQueue.indexOf(c.param === -1)) {
                        commandsQueue.push(
                            {   'callback': c.param.replace('(...)', ''),
                                'params': null,
                                'weight': weight,
                                'type': 'fn',
                                'extras': command[1]
                            }
                        );
                    }
                    else if (commandsQueue.indexOf(c.param === -1)) {
                        if (c.param.match(/\.\.\./g)) {
                            commandsQueue.push(
                                {   'callback': c.param.replace('...', ''),
                                    'params': null,
                                    'weight': weight,
                                    'type': 'cmd',
                                    'extras': command[1]
                                }
                            );
                        } else {
                            commandsQueue.push(
                                {   'callback': c.param,
                                    'weight': weight,
                                    'type': 'cmd',
                                    'extras': command[1]
                                }
                            );
                        }
                    }
                }
            }
        });

        return commandsQueue;
    };

    /**
     *  Find the heaviest element in array
     **/
    var findHeaviestElement = function (commandsQueue) {
        var maxWeight = 0;
        commandsQueue.forEach(function (item) {
            var weight = item.weight;
            if (weight > maxWeight) {
                maxWeight = weight;
            }
        });
        return maxWeight;
    };

    /**
     * Execute heaviest command
     *
     **/
    var execute = function (commandsQueue) {
        var counter = 0;
        var maxWeight = findHeaviestElement(commandsQueue);
        commandsQueue.forEach(function (item) {
            var weight = item.weight;
            if (maxWeight === weight && counter === 0) {

                if (item.params === undefined) {
                    switch (item.type) {
                        case 'cmd' :
                            exec(item.callback, puts);
                            break;
                        case 'fn' :
                            fn[item.callback]();
                            break;
                    }
                } else {
                    if (item.extras != undefined)
                        processMessage(item.extras);
                        //process message for 2.5 secs lets pause so Jarvis stop talking
                        setTimeout(function () {
                            currentCommand = item;
                            queryGoogleAPI(item);
                        }, 2500);
                }
                counter++;
            }
        });
    };

    /**
     * Records voice and query google. Google API returns transcript which is being used as
     * additional commands(functions) parameters.
     * Sample dialog :
     * U: Jarvis I have a question?
     * J: What would you like to know?
     * U: Whats the time in Poland
     * (queryGoogleApi execution) J:response
     * @param Object item
     */
    var queryGoogleAPI = function (item) {
        speakable.recordVoice();

        speakable.on('speechStop', function () {
            if (speaking === false)
                processMessage('Please wait ');
            speaking = true;
        });

        speakable.on('speechResult', function (spokenWords) {
            speaking = false;
            var cmd = spokenWords.join(" ");

            if (item.type == 'cmd') {
                exec(item.callback + cmd, puts);

            } else if (item.type == 'fn') {
                fn[item.callback]('"' + cmd + '"');
            }
        });
    };

    /**
     *  Main program loop
     *
     **/
    var mainLoop = function () {
        getCommands();
        pluginRegistration();
        getConfig();
        setTimeout(function () {
            welcome();
            //console.log(fn);
            //fn['movies']('play the originals episode 2 from season 1',function(e){console.log(e);});
	        app.listen(3000,'0.0.0.0');
            app.use(cors());
            app.use(bodyParser.json());
            app.use(bodyParser.urlencoded({'extended': 'true'}));
            app.use(express.static(__dirname));
            app.use(express.static(__dirname + '/public'));

            var server = http.createServer();
            echo.installHandlers(server, {prefix:'/echo'});
            server.listen(3001, '0.0.0.0');
            console.log('Server started.  Running at http://localhost: 3000');
            console.log('Socket started.  Running at http://localhost: 3001');
        }, 1000);

    };


    /**
     *  API calls
     *  @Todo Move to seperate module maybe
     */

    /**
     *  Post from Java(Sphinx) speech recognition
     */
    app.post('/jarvis', function (req, response) {
        if (speaking === false) {
            var fullBody = '';

            req.on('data', function (chunk) {
                fullBody += chunk.toString();
            });

            req.on('end', function () {
                // parse the received body data
                var decodedBody = JSON.parse(fullBody);
                commandsQueue = commandStack(decodedBody.data);
                execute(commandsQueue);

                response.send("Data received");
                response.end();
            });
        }
    });

    /**
     * Post from Angular Jarvis
     * Sets global speak variable to false so all plugin methods will return json object instead of process voice in RP
     * keeping with node's async nature we need to implement callbacks here and data can be easily send back to
     * angular.
     */
    app.post('/jarvis-mob', function(request,response){
        if (request.body.data) {
            commandsQueue = commandStack(request.body.data);
            var counter = 0;
            var maxWeight = findHeaviestElement(commandsQueue);
            commandsQueue.forEach(function (item) {
                var weight = item.weight;
                if (maxWeight === weight && counter === 0) {
                    if (item.params === undefined) {
                        switch (item.type) {
                            case 'cmd' :
                                exec(item.callback, puts);
                                break;
                            case 'fn' :
                                fn[item.callback](function(data){
                                    response.send(data);
                                });
                                break;
                        }
                    } else {
                        response.send(item.extras);
                    }
                    counter++;
                }
            });
        }
    });

    app.post('/jarvis-mob-additional-query', function(request,response){
        var item = request.body.data;
        if (item) {

            if (item.type == 'cmd') {
                exec(item.callback + cmd, puts);

            } else if (item.type == 'fn') {
                fn[item.callback]('"' + cmd + '"', function(data){
                    response.send(data);
                });
            }
        }
    });

    /**
     *  Returns voice commands list
     */
    app.get('/get-all-commands', function (request, response) {
        getCommands();
        setTimeout(function () {
            var obj = _commandsList();
            response.send(obj);
        }, 500);
    });

    /**
     * Gets general configuration options
     */
    app.get('/get-config', function (request, response) {
        getConfig();
        setTimeout(function () {
            var obj = _configList();
            response.send(obj);
        }, 500);
    });

    /**
     * Upload file to server
     */
    app.post('/', function (request, response) {

        if (request.method === 'POST') {
            var busboy = new Busboy({ headers: request.headers });
            var newPath = __dirname + "/plugins";
            var fn = '';
            busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
                fn = filename;
                var saveTo = path.join(newPath, path.basename(filename));

                file.pipe(fs.createWriteStream(saveTo));
            });
            busboy.on('finish', function () {
                response.writeHead(200, { 'Connection': 'close' });
                response.end("That's all folks!");
                fs.readFile(newPath + '/' + fn, 'utf8', function (err, data) {
                    if (err) throw err;
                    var eachLine = data.split('\n');
                    eachLine.forEach(function (obj) {
                        //install npm requirements
                        var install = obj.match(/(install =).*/gi)
                        if (install) {
                            //console.log(install);
                            var mod = install[0].split("=");
                            console.log("Installing module : " + mod[1]);
                            exec("npm install " + mod[1], puts);
                        }
                    });
                });

            });
            return request.pipe(busboy);
        }
        response.writeHead(404);
        response.end();

    });

    /**
     * Create new voice command
     */
    app.post('/create-command', function (request, response) {
        var command,
            question,
            exec,
            errors = 0,
            res = {'message': 'New command created!', 'success': true, errors: {}};

        //console.log(request.body);
        if (request.body.command != undefined && request.body.command !== '') {
            command = request.body.command;
        } else {
            res.errors.commandError = "Command is empty";
            res.success = false;
            res.message = "Error";
            errors = 1;
        }
        if (request.body.exec != undefined && request.body.exec !== '') {
            exec = request.body.exec;
        } else {
            res.errors.shellError = "Execution script is empty";
            res.message = "Error";
            res.success = false;
            errors = 1;
        }

        question = (request.body.question != undefined) ? request.body.question : '';

        if (errors === 0) {
            var str = '\n' + command + '|' + question + '==' + exec;

            fs.appendFile(commandsFile, str, function (err) {
                if (err) return console.log(err);
            });

        }
        response.send(res);
    });

    /**
     * Gets form data and update voice command based on oldCommand parameter
     */
    app.post('/get-command', function (request, response) {
        var res = {'message': 'Cannot find command', 'success': false, errors: {}};
        if (request.body.command != undefined) {
            commands.forEach(function (c, k) {
                var command = c.command.split("|");
                if (command[0].match(new RegExp(request.body.command, "ig"))) {
                    var que = (command[1] !== 'undefined') ? command[1] : '';
                    var com = command[0];
                    var par = c.param;
                    res.message = {'command': com, 'question': que, 'shell': par};
                    res.success = true;
                }
            });
        }
        response.send(res);
    });

    /**
     * Edit voice command
     */
    app.post('/edit-command', function (request, response) {
        var command,
            question,
            exec,
            errors = 0,
            res = {'message': 'Command updated!', 'success': true, errors: {}};

        if (request.body.command != undefined && request.body.command !== '') {
            command = request.body.command;
        } else {
            res.errors.commandError = "Command is empty";
            res.success = false;
            res.message = "Error with updating command";
            errors = 1;
        }
        if (request.body.exec != undefined && request.body.exec !== '') {
            exec = request.body.exec;
        } else {
            res.errors.shellError = "Execution script is empty";
            res.message = "Error with updating command";
            res.success = false;
            errors = 1;
        }

        question = (request.body.question != undefined) ? request.body.question : '';

        var str = command + "|" + question + "==" + exec;

        if (errors === 0) {
            var commandBeingUpdated = request.body.orginalCommandName;

            fs.readFile(commandsFile, 'utf8', function (err, data) {
                if (err) {
                    return console.log(err);
                }
                var newRgx = new RegExp('(' + commandBeingUpdated + ').*', 'g');
                var result = data.replace(newRgx, str);
                res.orginalCommandName = command;
                fs.writeFile(commandsFile, result, 'utf8', function (err) {
                    if (err) return console.log(err);
                });
            });
        }
        response.send(res);
    });

    /**
     * Delete command
     */
    app.post('/delete-command', function (request, response) {
        var commandBeingUpdated = request.body.orginalCommandName,
            str = '',
            command = request.body.command,
            res = {'message': 'Command deleted!', 'success': true, errors: {}};

        fs.readFile(commandsFile, 'utf8', function (err, data) {
            if (err) {
                return console.log(err);
            }
            var newRgx = new RegExp('[\n\r]+(' + commandBeingUpdated + ').*', 'g');
            var result = data.replace(newRgx, str);
            res.orginalCommandName = command;
            fs.writeFile(commandsFile, result, 'utf8', function (err) {
                if (err) return console.log(err);
            });
        });
        response.send(res);
    });

    /**
     * Edit config
     */
    app.post('/edit-config', function (request, response) {
        var voice,
            email,
            password
        errors = 0,
            res = {'message': 'Config Updated!', 'success': true, errors: {}};

        if (request.body.email != undefined && request.body.email !== '') {
            email = request.body.email;
        } else {
            res.errors.email = "Email cannot be empty!";
            res.success = false;
            res.message = "Error with updating email";
            errors = 1;
        }
        if (request.body.voice != undefined && request.body.voice !== '') {
            voice = request.body.voice;
        } else {
            res.errors.voice = "Voice cannot be empty!";
            res.message = "Error with updating voice";
            res.success = false;
            errors = 1;
        }
        if (request.body.password != undefined && request.body.password !== '') {
            password = request.body.password;
        } else {
            res.errors.password = "Password cannot be empty!";
            res.message = "Error with updating password";
            res.success = false;
            errors = 1;
        }

        if (errors === 0) {
            var result = '';
            var c = 0;
            _.forEach(request.body, function (c, k) {
                var breaker = '\n';
                if (k === 'selectedOption') {
                    result += 'voice' + '=' + c.name + breaker;
                } else if (k !== 'options' && k !== 'voice') {
                    result += k + '=' + c + breaker
                }
                c++;
            });

            fs.writeFile(configFile, result, 'utf8', function (err) {
                if (err) return console.log(err);
            });
        }

        response.send(res);

    });

    var echo = sockjs.createServer();
    echo.on('connection', function(conn) {

        var term = pty.spawn('bash', [], {
            name: 'xterm-color',
            cols: 80,
            rows: 30,
            cwd: process.env.HOME,
            env: process.env
        });

        term.on('data', function(data) {
            console.log(data);
            conn.write(data);
        });

        conn.on('data',function(message) {
            if (message==='cls'){
                term.write('\n');

            } else {
                console.log('Input :' + message);
                var proc = term.write(message+'\r');
            }
        });

        conn.on('close', function() {
            term.kill();
        });
    });


    /**
     * Private API helpers
     * @type {number}
     */

    var _configList = function () {
        var conf = {};

        _.forEach(config, function (c, k) {
            conf[k] = c;
        });
        return conf;
    };

    var _commandsList = function () {
        var obj = [];
        commands.forEach(function (c, k) {
            var command = c.command.split("|");
            obj.push(command[0]);
        });
        return obj;
    };

    mainLoop();

})();

